#ifndef DEFS_INCLUDED_H
#define DEFS_INCLUDED_H

enum errCodes
{
    MEMORY_ALLOC_ERR = -5,  /* Erreur d'allocation mémoire */
    BAD_ARGUMENT,   /* Mauvais argument */
};

enum Types
{
    EAU = 1,
    FEU,
    PLANTE,
    ELEC,
    SOL,
    DRAGON,
    NORMAL,
    PSY,
    INSECTE,
    VOL,
};

enum Pokemons
{
    ASPICOT = 100,
	BULBIZARRE,
    CARAPUCE,
	DRACO,
	MAGICARPE,
	MEW,
    PIKACHU,
	RONDOUDOU,
	ROUCOOL,
	SALAMECHE,
	TAUPIQUEUR,
};

#endif