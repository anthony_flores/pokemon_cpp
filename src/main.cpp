#include <iostream>
#include <time.h>

#include "defs.h"
#include "Dresseur.h"
#include "Pokemon.h"
#include "Attack.h"
#include "PPikachu.h"
#include "PCarapuce.h"
#include "PAspicot.h"
#include "PBulbizarre.h"
#include "PDraco.h"
#include "PMagicarpe.h"
#include "PMew.h"
#include "PRondoudou.h"
#include "PRoucool.h"
#include "PSalameche.h"
#include "PTaupiqueur.h"

#include "TEau.h"
#include "TFeu.h"
#include "TPlante.h"
#include "TElec.h"
#include "TSol.h"
#include "TDragon.h"
#include "TNormal.h"
#include "TPsy.h"
#include "TInsecte.h"
#include "TVol.h"

using namespace std;

int yesorno_input = 0;
Pokemon *poke1, *poke2;
Dresseur* dresseur;

int yesorno(){
	int yesorno_input = 0;
	char choice;
	std::cout << "Y\\N : ";
	std::cin >> choice;

	if (choice == 'y' || choice == 'Y')
		yesorno_input = 1;
	else if (choice == 'n' || choice == 'N')
		yesorno_input = 0;
	else
		std::cout << "...";
	std::cin.get();
	return yesorno_input;
}

int fgame(){
	std::cout << "Voulez-vous jouer une partie ?\n";

	yesorno_input = yesorno();

	if (yesorno_input == 1)
		std::cout << "\nLa partie va commencer !!!\n";
	else
		return EXIT_FAILURE;

	//td::cin.get();
	return EXIT_SUCCESS;
}

int fchoice(Pokemon *poke1, Pokemon *poke2){

	int x;
	std::cout << "Quel Pokemon voulez-vous envoyer (X)? \n";
	std::cout << "1 : " + poke1->getName() + "\n2 : " + poke2->getName() + "\n" ;
	std::cin >> x;

	switch(x)
	{
		case 1:
			std::cout << "Vous avez choisis " + poke1->getName() + "";
			break;
		case 2:
			std::cout << "Vous avez choisis " + poke2->getName() + "";
			break;
	}
	return EXIT_SUCCESS;
}

int16_t mainLoop(Dresseur* dresseur);

int main()
{
	int16_t rc = 0;
	
	Dresseur* dresseur = new Dresseur();
	/*
	Pikachu* pikachu = nullptr;
    pikachu = new Pikachu();
	dresseur->ajouterPokemon(pikachu);
	*/
	rc = mainLoop(dresseur);
	delete dresseur;

	if (rc != EXIT_SUCCESS)
		return rc;
	return EXIT_SUCCESS;
}

int fight(Dresseur* dresseur, Pokemon* pokemon)
{
	Pokemon* premierPokemon = dresseur->premierPokemon();
	Pokemon* remplacement = nullptr;
	Pokemon tmp;
	int16_t res = 0;
	int32_t size = 0;
	int16_t choice = 0;

	cout << "\nUn " << pokemon->getName() << " sauvage apparait !\n\n";

	while (!pokemon->getKo())
	{
		cout << "1) Afficher les attaques\n2) Changer de pokemon\n3) Soigner le pokemon \n4) Attraper le pokemon\n5) Fuir\n";
		cin >> res;
		switch (res)
		{
		case 1:
			size = premierPokemon->array_attack.size();
			for (int i = 0; i < size; i++)
			{
				cout << i+1 << ") " << premierPokemon->array_attack[i]->toString() << "\n";
			}			
			cin >> choice;
            if(!choice)
                break;

            
            choice -= 1;
			if(choice > size)
                break;

            premierPokemon->attack(pokemon, premierPokemon->array_attack[choice]);
            pokemon->attack(premierPokemon, pokemon->array_attack[rand() % 2]);
            
            cout << premierPokemon->getName() << ": " << premierPokemon->getHp() << "HP" << "\n";
            cout << pokemon->getName() << ": " << pokemon->getHp() << "HP" << "\n";
            
            break;
		case 2:
			size = dresseur->getSizeList();
			for (int i = 0; i < size; i++)
			{
				cout << i+1 << ") " << dresseur->getPokemon(i)->getName() << "\n";
			}
			cin >> choice;

			remplacement = dresseur->getPokemon(choice - 1);

			tmp = *premierPokemon;
			*premierPokemon = *remplacement;
			*remplacement = tmp;
			break;
		case 3:
			cout << "1) Potion 10 PV\n2) Potion 50 PV\n";
			cin >> choice;

			switch (choice)
			{
			case 1:
				premierPokemon->heal(10);
				cout << "Le pokemon est soigné de 10 PV\n\n";				
				break;
			case 2:
				premierPokemon->heal(50);
				cout << "Le pokemon est soigné de 50 PV\n\n";				
				break;
			default:
				break;
			}
            
            cout << premierPokemon->getName() << ": " << premierPokemon->getHp() << "HP" << "\n";
            

			break;	
		case 4:
			if(pokemon->getHp() > pokemon->getMaxHp()/2) {
				cout << "Le Pokemon se libere!\n";
			}
			else {
				cout << "Le Pokemon est attrapé\n!";
				dresseur->ajouterPokemon(pokemon); /* Capture garantie */
                return EXIT_SUCCESS;
			}
			break;
		case 5:
			return EXIT_SUCCESS;
			break;
		default:
			break;
		}
	}





	return EXIT_SUCCESS;
}

int16_t mainLoop(Dresseur* dresseur)
{
	int16_t rc = 0;
	int16_t res = 0;
	int16_t pokemonMet = 0;
	int16_t result = 0;


    Type* typeEau = new TypeEau();
    Type* typeFeu = new TypeFeu();
    Type* typePlante = new TypePlante();
    Type* typeElec = new TypeElec();
    Type* typeSol = new TypeSol();
    Type* typeDragon = new TypeDragon();
    Type* typeNormal = new TypeNormal();
    Type* typePsy = new TypePsy();
    Type* typeInsecte = new TypeInsecte();
    Type* typeVol = new TypeVol();

	
	Aspicot* aspicot = new Aspicot(typeInsecte);
	Bulbizarre* bulbizarre = new Bulbizarre(typePlante);
	Carapuce* carapuce = new Carapuce(typeEau);
	Draco* draco = new Draco(typeDragon);
	Magicarpe* magicarpe = new Magicarpe(typeEau);
	Mew* mew = new Mew(typePsy);
	Pikachu* pik = new Pikachu(typeElec);
	Rondoudou* rondoudou = new Rondoudou(typeNormal);
	Roucool* roucool = new Roucool(typeVol);
	Salameche* salameche = new Salameche(typeFeu);
	Taupiqueur* taupiqueur = new Taupiqueur(typeSol);


	srand(time(NULL));

	/* On check si on peut lancer la partie */
	rc = fgame();

	if (!dresseur)
		return BAD_ARGUMENT;

	if (rc != EXIT_SUCCESS)
		return rc;
	
	cout << "Choisissez votre Pokemon starter :\n1) Salameche\n2) Carapuce\n3) Bulbizarre\n";
	cin >> result;

	if(result == 1){
		Salameche* salameche = nullptr;
		salameche = new Salameche(typeFeu);
		dresseur->ajouterPokemon(salameche);
	}else if(result == 2){
		Carapuce* carapuce = nullptr;
		carapuce = new Carapuce(typeEau);
		dresseur->ajouterPokemon(carapuce);
	}else if (result == 3){
		Bulbizarre* bulbizarre = nullptr;
		bulbizarre = new Bulbizarre(typePlante);
		dresseur->ajouterPokemon(bulbizarre);
	}

	cout << "Vous avez choisi \n" + dresseur->liste[0]->getName() + "\n";



	while (true)
	{
		cout << "\n1) Afficher liste de pokemon.\n2) Chercher un combat.\n3) Arreter la partie.\n";
		cin >> res;

		switch (res)
		{
		case 1:
			dresseur->printList();
			break;
		case 2:
			pokemonMet = rand() % 11 + 1;
			pokemonMet += 100;
			switch (pokemonMet)
			{
			case ASPICOT:
				fight(dresseur, aspicot);
				break;
			case CARAPUCE:
				fight(dresseur, carapuce);
				break;
			case BULBIZARRE:
				fight(dresseur, bulbizarre);
				break;
			case DRACO:
				fight(dresseur, draco);
				break;
			case MAGICARPE:
				fight(dresseur, magicarpe);
				break;
			case MEW:
				fight(dresseur, mew);
				break;
			case PIKACHU:
				fight(dresseur, pik);
				break;
			case RONDOUDOU:
				fight(dresseur, rondoudou);
				break;
			case ROUCOOL:
				fight(dresseur, roucool);
				break;
			case SALAMECHE:
				fight(dresseur, salameche);
				break;
			case TAUPIQUEUR:
				fight(dresseur, taupiqueur);
				break;
			default:
				break;
				// pokemonMet = rand() % 10 + 1; /* Nombre aléatoire dans l'intervalle [1, 10] */
				// cout << "Carapute sauvage\n";


				// /* On récupère le premier pokémon de l'équipe */
				// premierPokemon = dresseur->premierPokemon();
				// premierPokemon->viewAttack();
				// // premierPokemon->attack(carapuce, premierPokemon->)
				// cin.get();
			}

			cout << "SOIN !!! \n";
			for (int i = 0; i < dresseur->getSizeList(); i++)
			{
				dresseur->getPokemon(i)->heal(dresseur->getPokemon(i)->getMaxHp());
				cout << dresseur->getPokemon(i)->getName() + " est soigner au max\n";
			}

			break;
		case 3:
			return EXIT_SUCCESS;
			break;
		default:
			break;
		}
	}
}