#ifndef TYPE_H
#define TYPE_H

#include <iostream>
#include <vector>
#include <string>

#include "defs.h"

using namespace std;

class Type {
protected:
    string name;
    int weekness;
    int resistance;
    int typeId;

public:
    Type(string name, int typeId, int weekness, int resistance);
    ~Type();

    string toString() const;

    string getName();
    int getWeekness();
    int getResistance();
    int getTypeId();

    void setName(string newName);
    void setWeekness(int newType);
    void setResistance(int newType);
    void setTypeId(int newTypeId);
};

#endif