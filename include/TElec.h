#ifndef TYPE_ELEC_INCLUDED_H
#define TYPE_ELEC_INCLUDED_H

#include "Type.h"
#include "defs.h"

class TypeElec : public Type {
public:
	TypeElec() : Type("Elec", ELEC, FEU, EAU) {};
	~TypeElec();
};
#endif