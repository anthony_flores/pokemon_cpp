#ifndef TYPE_DRAGON_INCLUDED_H
#define TYPE_DRAGON_INCLUDED_H

#include "Type.h"
#include "defs.h"

class TypeDragon : public Type {    
public:
    TypeDragon() : Type("Dragon", DRAGON, DRAGON, EAU){};
    ~TypeDragon();
};
#endif