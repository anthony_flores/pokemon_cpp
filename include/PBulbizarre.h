#ifndef BULBIZARRE_H
#define BULBIZARRE_H

#include <iostream>
#include <vector>

#include "Attack.h"
#include "Type.h"
#include "Pokemon.h"

using namespace std;

class Bulbizarre : public Pokemon{
protected:
    string name;
    int type;
    vector<Attack*> attacks;

public:
    Bulbizarre(Type* type) : Pokemon("Bulbizarre", type){
        Pokemon::addAttack(new Attack("Fouet Lianes", type, 45));
        Pokemon::addAttack(new Attack("Tranch'Herbe", type, 55));
    };
    ~Bulbizarre();

    string toString() const;

    string getName();
    Type getType();

    void setName(string newName);
    void setType(int newType);
};

#endif