#ifndef MAGICARPE_H
#define MAGICARPE_H

#include <iostream>
#include <vector>

#include "Attack.h"
#include "Type.h"
#include "Pokemon.h"

using namespace std;

class Magicarpe : public Pokemon{
protected:
    string name;
    int type;
    vector<Attack*> attacks;

public:
    Magicarpe(Type* type) : Pokemon("Magicarpe", type){
        Pokemon::addAttack(new Attack("Charge", type, 40));
        Pokemon::addAttack(new Attack("Trempette", type, 0));
    };
    ~Magicarpe();

    string toString() const;

    string getName();
    Type getType();

    void setName(string newName);
    void setType(int newType);
};

#endif