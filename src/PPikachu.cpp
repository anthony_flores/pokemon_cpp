#include "PPikachu.h"
#include "Attack.h"

#include <iostream>
#include <string>
#include <Type.h>

using namespace std;

// Pikachu::Pikachu(){}

string Pikachu::toString() const{
    return this->name + " de type " + to_string(this->type) + " à " + to_string(this->attacks.size()) + " attaques";
}

Pikachu::~Pikachu(){}