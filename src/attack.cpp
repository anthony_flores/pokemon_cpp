#include "Attack.h"

#include <iostream>
#include "Type.h"
using namespace std;

Attack::Attack(string name, Type* type, int attack_value){
    this->name = name;
    this->type = type;
    this->attack_value = attack_value;
}

Attack::Attack(){}
Attack::Attack(Attack const& attack){
    this->attack_value = attack.attack_value;
    this->name = attack.name;
    this->type = attack.type;
}

string Attack::toString() const {
    string strType = this->type->toString();
    // switch (this->type)
    // {
    // case EAU:
    //     strType = "Eau";
    //     break;
    // case FEU:
    //     strType = "Feu";
    //     break;
    // case PLANTE:
    //     strType = "Plante";
    //     break;
    // case ELEC:
    //     strType = "Electrique";
    //     break;
    // case SOL:
    //     strType = "Sol";
    //     break;
    // case DRAGON:
    //     strType = "Dragon";
    //     break;
    // case NORMAL:
    //     strType = "Normal";
    //     break;
    // case PSY:
    //     strType = "Psy";
    //     break;
    // case INSECTE:
    //     strType = "Insecte";
    //     break;
    // case VOL:
    //     strType = "Vol";
    //     break;
    
    // default:
    //     break;
    // }
    return "Attack : \n Name : " + this->name + " \n Type : " + strType + " \n value : " +  std::to_string(this->attack_value) + "";
}

string Attack::getName(){ return name; }
Type* Attack::getType(){ return type; }
int Attack::getAttack_value(){ return attack_value; }

void Attack::setName(string newName){ name = newName; }
void Attack::setType(Type* newType){ type = newType;} 
void Attack::setAttack_value(int newAttack_value){ attack_value = newAttack_value; }
