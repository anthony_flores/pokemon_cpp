#include "PDraco.h"
#include "Attack.h"

#include <iostream>
#include <string>
#include <Type.h>

using namespace std;

// Draco::Draco(){}

string Draco::toString() const{
    return this->name + " de type Dragon à " + to_string(this->attacks.size()) + " attaques";
}

Draco::~Draco(){}