#ifndef TYPE_NORML_INCLUDED_H
#define TYPE_NORML_INCLUDED_H

#include "Type.h"
#include "defs.h"


class TypeNormal : public Type {
public:
	TypeNormal() : Type("Normal", NORMAL, NORMAL, SOL){};
	~TypeNormal();
};
#endif