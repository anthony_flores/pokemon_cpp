#ifndef TAUPIQUEUR_H
#define TAUPIQUEUR_H

#include <iostream>
#include <vector>

#include "Attack.h"
#include "Type.h"
#include "Pokemon.h"

using namespace std;

class Taupiqueur : public Pokemon{
protected:
    string name;
    int type;
    vector<Attack*> attacks;

public:
    Taupiqueur(Type* type) : Pokemon("Taupiqueur", type){
        Pokemon::addAttack(new Attack("Coud'Boue", type, 20));
        Pokemon::addAttack(new Attack("Boue-Bombe", type, 50));
    };
    ~Taupiqueur();

    string toString() const;

    string getName();
    Type getType();

    void setName(string newName);
    void setType(int newType);
};

#endif