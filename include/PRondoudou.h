#ifndef RONDOUDOU_H
#define RONDOUDOU_H

#include <iostream>
#include <vector>

#include "Attack.h"
#include "Type.h"
#include "Pokemon.h"

using namespace std;

class Rondoudou : public Pokemon{
protected:
    string name;
    int type;
    vector<Attack*> attacks;

public:
    Rondoudou(Type* type) : Pokemon("Rondoudou", type){
        Pokemon::addAttack(new Attack("Écras'Face", type, 40));
        Pokemon::addAttack(new Attack("Chant Canon", type, 90));
    };
    ~Rondoudou();

    string toString() const;

    string getName();
    Type getType();

    void setName(string newName);
    void setType(int newType);
};

#endif