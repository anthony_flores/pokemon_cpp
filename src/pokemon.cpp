#include <iostream>

#include "Pokemon.h"
#include "defs.h"

using namespace std;

Pokemon::Pokemon(){}

Pokemon::Pokemon(string name, Type* type){
    this->name = name;
    this->type = type;
    this->maxHp = 100;
    this->hp = 100;
    this->isKo = false;
}

Pokemon::Pokemon(string name, Type* type, int maxHp){
    this->name = name;
    this->type = type;
    this->maxHp = maxHp;
    this->hp = maxHp;
    this->isKo = false;
}

Pokemon::Pokemon(Pokemon const& pokemon){
    this->gender = pokemon.gender;
    this->name = pokemon.name;
    this->type = pokemon.type;
    this->array_attack = pokemon.array_attack;
    this->maxHp = pokemon.maxHp;
    this->hp = pokemon.hp;
    this->isKo = pokemon.isKo;
}

Pokemon::~Pokemon(){}

string Pokemon::toString() const {
    return "Pokemon : \n " + this->name + "\n";
}

string Pokemon::getName(){ return name; }
string Pokemon::getGender(){ return gender;}
Type* Pokemon::getType(){ return type;}

int Pokemon::getHp(){return hp;}
int Pokemon::getMaxHp(){return maxHp;}
bool Pokemon::getKo(){return isKo;}


void Pokemon::setName(string newName){name = newName;}
void Pokemon::setGender(string newGender){gender = newGender;}
void Pokemon::setType(Type* newType){type = newType;}

void Pokemon::addAttack( Attack* attack){ this->array_attack.push_back(attack); }

void Pokemon::viewAttack(){
	std::cout << "La liste des attaques de " + name + " : " << endl;
    int32_t size = this->array_attack.size();
    for (int i = 0; i < size; i++)
    {
        cout << array_attack[i]->toString() << "\n";
    }
    // for(auto const & it : this->array_attack){
    //  std::cout << it << endl;
    // }
}
void Pokemon::setHp(int newHp){hp = newHp;}
void Pokemon::setMaxHp(int newHp){hp = newHp;}
void Pokemon::takeDamage(int amount){
    hp -= amount;
    if(hp <= 0){
        hp = 0;
        isKo = true;
    }
}

void Pokemon::heal(int amount){
    hp += amount;
    isKo = false;
    if(hp > maxHp)
        hp = maxHp;
}

int16_t Pokemon::attack(Pokemon* pokemon, Attack* attack){
    if (!pokemon || !attack)
        return BAD_ARGUMENT;

    int damageTaken = attack->getAttack_value();

    if(attack->getType()->getTypeId() == pokemon->type->getWeekness())
        damageTaken *= 2;
    if(attack->getType()->getTypeId() == pokemon->type->getResistance())
        damageTaken /= 2;
    
    
    pokemon->takeDamage(damageTaken);
    return pokemon->getHp();
}

void Pokemon::setKo(bool newKo){isKo = newKo;}