#include "PMew.h"
#include "Attack.h"

#include <iostream>
#include <string>
#include <Type.h>

using namespace std;

// Mew::Mew(){}

string Mew::toString() const{
    return this->name + " de type Psy à " + to_string(this->attacks.size()) + " attaques";
}

Mew::~Mew(){}