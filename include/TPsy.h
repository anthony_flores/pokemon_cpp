#ifndef TYPE_PSY_INCLUDED_H
#define TYPE_PSY_INCLUDED_H

#include "Type.h"
#include "defs.h"


class TypePsy : public Type {
public:
	TypePsy() : Type("Psy", PSY, PSY, INSECTE){};
	~TypePsy();
};
#endif