#include "PRondoudou.h"
#include "Attack.h"

#include <iostream>
#include <string>
#include <Type.h>

using namespace std;

// Rondoudou::Rondoudou(){}

string Rondoudou::toString() const{
    return this->name + " de type Normal à " + to_string(this->attacks.size()) + " attaques";
}

Rondoudou::~Rondoudou(){}