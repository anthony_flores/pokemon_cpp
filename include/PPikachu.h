#ifndef PIKACHU_H
#define PIKACHU_H

#include <iostream>
#include <vector>

#include "Attack.h"
#include "Type.h"
#include "Pokemon.h"

using namespace std;

class Pikachu : public Pokemon{
protected:
    string name;
    int16_t type;
    vector<Attack*> attacks;

public:
    Pikachu(Type* type) : Pokemon("Pikachu", type){
        Pokemon::addAttack(new Attack("Charge", type, 15));
        Pokemon::addAttack(new Attack("Eclair", type, 25));
    };
    ~Pikachu();

    string toString() const;

    string getName();
    Type getType();

    void setName(string newName);
    void setType(int newType);
};

#endif