#ifndef TYPE_SOL_INCLUDED_H
#define TYPE_SOL_INCLUDED_H

#include "Type.h"
#include "defs.h"


class TypeSol : public Type {
public:
	TypeSol() : Type("Sol", SOL, FEU, INSECTE){};
	~TypeSol();
};
#endif