#ifndef CARAPUCE_H
#define CARAPUCE_H

#include <iostream>
#include <vector>

#include "Attack.h"
#include "Type.h"
#include "Pokemon.h"

using namespace std;

class Carapuce : public Pokemon{
protected:
    string name;
    int type;
    vector<Attack*> attacks;

public:
    Carapuce(Type* type) : Pokemon("Carapuce", type){
        Pokemon::addAttack(new Attack("Pistolet à O", type, 40));
        Pokemon::addAttack(new Attack("Hydroqueue", type, 90));
    };
    ~Carapuce();

    string toString() const;

    string getName();
    Type getType();

    void setName(string newName);
    void setType(int newType);
};

#endif