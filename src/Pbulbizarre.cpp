#include "PBulbizarre.h"
#include "Attack.h"

#include <iostream>
#include <string>
#include <Type.h>

using namespace std;

// Bulbizarre::Bulbizarre(){}

string Bulbizarre::toString() const{
    return this->name + " de type Plante à " + to_string(this->attacks.size()) + " attaques";
}

Bulbizarre::~Bulbizarre(){}