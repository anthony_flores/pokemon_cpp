#include "PMagicarpe.h"
#include "Attack.h"

#include <iostream>
#include <string>
#include <Type.h>

using namespace std;

// Magicarpe::Magicarpe(){}

string Magicarpe::toString() const{
    return this->name + " de type Eau à " + to_string(this->attacks.size()) + " attaques";
}

Magicarpe::~Magicarpe(){}