#include "Dresseur.h"

Dresseur::Dresseur()
{

}

int32_t Dresseur::ajouterPokemon(Pokemon* pokemon)
{
    /* Vérification d'usage */
    if (!pokemon)
        return BAD_ARGUMENT; /* pokemon non définie, on s'arrête */
    
    this->liste.push_back(pokemon);
    return EXIT_SUCCESS;
}

void Dresseur::printList()
{
    int32_t size = this->liste.size();
    cout << "Pokemon :\n";

    for (int i = 0; i < size; i++)
        cout << this->liste[i]->getName() + "\n";
}

Dresseur::~Dresseur()
{
    int32_t size = this->liste.size();

    for (int i = 0; i < size; i++)
        delete this->liste[i];
}

int32_t Dresseur::getSizeList()
{
    return this->liste.size();
}

Pokemon* Dresseur::getPokemon(int pos)
{
    return this->liste[pos];
}

int16_t Dresseur::setPokemon(Pokemon* pok, int pos)
{
    if (!pok)
        return BAD_ARGUMENT;
        
    this->liste[pos] = pok;
    return EXIT_SUCCESS;
}

Pokemon* Dresseur::premierPokemon()
{
    int16_t size = this->liste.size();
    if (size > 0)
        return this->liste[0];
    else
        return NULL;
}