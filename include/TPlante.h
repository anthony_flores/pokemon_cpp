#ifndef TYPE_PLANTE_INCLUDED_H
#define TYPE_PLANTE_INCLUDED_H

#include "Type.h"
#include "defs.h"

class TypePlante : public Type {
public:
    TypePlante() : Type("Plante", PLANTE, FEU, EAU){};
    ~TypePlante();
};
#endif