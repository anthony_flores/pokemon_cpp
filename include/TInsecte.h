#ifndef TYPE_INSCT_INCLUDED_H
#define TYPE_INSCT_INCLUDED_H

#include "Type.h"
#include "defs.h"


class TypeInsecte : public Type {    
public:
    TypeInsecte() : Type("Insecte", INSECTE, FEU, PLANTE){};
    ~TypeInsecte();
};


#endif