#ifndef SALAMECHE_H
#define SALAMECHE_H

#include <iostream>
#include <vector>

#include "Attack.h"
#include "Type.h"
#include "Pokemon.h"
#include "defs.h"

using namespace std;

class Salameche : public Pokemon{
protected:
    string name;
    int type;
    vector<Attack*> attacks;

public:
    Salameche(Type* type) : Pokemon("Salameche", type){
        Pokemon::addAttack(new Attack("Flammèche", type, 40));
        Pokemon::addAttack(new Attack("Crocs Feu", type, 65));
    };
    ~Salameche();

    string toString() const;

    string getName();
    Type getType();

    void setName(string newName);
    void setType(int newType);
};

#endif