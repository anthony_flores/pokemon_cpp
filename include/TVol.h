#ifndef TYPE_VOL_INCLUDED_H
#define TYPE_VOL_INCLUDED_H

#include "Type.h"
#include "defs.h"


class TypeVol : public Type {
public:
	TypeVol() : Type("Vol", VOL, PLANTE, ELEC){};
	~TypeVol();
};
#endif