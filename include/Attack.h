#ifndef ATTACK_H
#define ATTACK_H

#include <iostream>
#include <string>
#include <vector>
#include "Type.h"
using namespace std;

class Attack
{
protected:
    /* data */
    string name;
    Type* type;
    int attack_value;

public:
    Attack();
    Attack(string name, Type* type, int attack_value);
    Attack(Attack const& attack);
    ~Attack();

    string toString() const;

    /* string getName()
     * Permet d'obtenir le nom d'une attaque.
     */
    string getName();

    /* string getType()
     * Permet d'obtenir le type d'un attaque.
     */
    Type* getType();

    /* int getAttack_value()
     * Permet d'obtenir la valeur d'une attaque.
     */
    int getAttack_value();

    /* void setName(string newName)
     * Permet de définir un nom pour une attaque.
     */
    void setName(string newName);

    /* void serType(string newType)
     * Permet de définir un type pour une attaque.
     */
    void setType(Type* newType);

    /* void setAttack_value(int newAttack_value)
     * Permet de définir une valeur pour une attaque.
     */
    void setAttack_value(int newAttack_value);
};

#endif