#ifndef DRESSEUR_INCLUDED_H
#define DRESSEUR_INCLUDED_H

#include <vector>

#include "defs.h"
#include "Pokemon.h"

class Dresseur
{
private:
    //std::vector<Pokemon*> liste;
public:
    std::vector<Pokemon*> liste;

    Dresseur();
    ~Dresseur();

    /* int32_t ajouterPokemon(Pokemon* pokemon)
     * Permet d'ajouter un pokémon à la liste de pokémon du joueur.
     * 
     * Pokemon* pokemon: Pointeur sur un objet Pokemon.
     * 
     * Renvoie 0 si tout s'est bien passé, un code d'erreur (<0) sinon.
     */
    int32_t ajouterPokemon(Pokemon* pokemon);

    /* void printList()
     * Fonction utilitaire permettant d'afficher la liste des pokémons du joueur.
     */
    void printList();
    Pokemon* getPokemon(int pos);
    int32_t getSizeList();

    int16_t setPokemon(Pokemon* pok, int pos);
    Pokemon* premierPokemon();
};
#endif