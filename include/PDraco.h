#ifndef DRACO_H
#define DRACO_H

#include <iostream>
#include <vector>

#include "Attack.h"
#include "Type.h"
#include "Pokemon.h"

using namespace std;

class Draco : public Pokemon{
protected:
    string name;
    int type;
    vector<Attack*> attacks;

public:
    Draco(Type* type) : Pokemon("Draco", type){
       Pokemon::addAttack(new Attack("Dracocharge", type, 100));
       Pokemon::addAttack(new Attack("Ouragan", type, 40));
    };
    ~Draco();

    string toString() const;

    string getName();
    Type getType();

    void setName(string newName);
    void setType(int newType);
};

#endif