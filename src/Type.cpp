#include "Attack.h"

#include <iostream>
#include <string>
using namespace std;


Type::Type(string name, int typeId, int weekness, int resistance)
{
    this->name = name;
    this->typeId = typeId;
    this->weekness = weekness;
    this->resistance = resistance;
}

Type::~Type(){

}

string Type::toString() const{
    return name;
}

string Type::getName() {return name;}
int Type::getWeekness() {return weekness;}
int Type::getResistance() {return resistance;}
int Type::getTypeId() {return typeId;}

void Type::setName(string newName) {name = newName;}
void Type::setWeekness(int newType) {weekness = newType;}
void Type::setResistance(int newType) {resistance = newType;}
void Type::setTypeId(int newTypeId) {typeId = newTypeId;}