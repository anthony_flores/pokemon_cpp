#ifndef ROUCOOL_H
#define ROUCOOL_H

#include <iostream>
#include <vector>

#include "Attack.h"
#include "Type.h"
#include "Pokemon.h"

using namespace std;

class Roucool : public Pokemon{
protected:
    string name;
    int type;
    vector<Attack*> attacks;

public:
    Roucool(Type* type) : Pokemon("Roucool", type){
        Pokemon::addAttack(new Attack("Tornade", type, 40));
        Pokemon::addAttack(new Attack("Lame d'Air", type, 75));
    };
    ~Roucool();

    string toString() const;

    string getName();
    Type getType();

    void setName(string newName);
    void setType(int newType);
};

#endif