#ifndef TYPE_FEU_INCLUDED_H
#define TYPE_FEU_INCLUDED_H

#include "Type.h"
#include "defs.h"

class TypeFeu : public Type {
public:
    TypeFeu() : Type("Feu", FEU, EAU, PLANTE){};
    ~TypeFeu();
};
#endif