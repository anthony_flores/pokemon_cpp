#ifndef ASPICOT_H
#define ASPICOT_H

#include <iostream>
#include <vector>

#include "Attack.h"
#include "Type.h"
#include "Pokemon.h"

using namespace std;

class Aspicot : public Pokemon{
protected:
    string name;
    int type;
    vector<Attack*> attacks;

public:
    Aspicot(Type* type) : Pokemon("Aspicot", type){
        Pokemon::addAttack(new Attack("Piqûre", type, 60));
        Pokemon::addAttack(new Attack("Plaie Croix", type, 80));
    };
    ~Aspicot();

    string toString() const;

    string getName();
    Type getType();

    void setName(string newName);
    void setType(int newType);
};

#endif