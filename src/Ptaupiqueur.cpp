#include "PTaupiqueur.h"
#include "Attack.h"

#include <iostream>
#include <string>
#include <Type.h>

using namespace std;

// Taupiqueur::Taupiqueur(){}

string Taupiqueur::toString() const{
    return this->name + " de type Sol à " + to_string(this->attacks.size()) + " attaques";
}

Taupiqueur::~Taupiqueur(){}