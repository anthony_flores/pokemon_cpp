#ifndef MEW_H
#define MEW_H

#include <iostream>
#include <vector>

#include "Attack.h"
#include "Type.h"
#include "Pokemon.h"

using namespace std;

class Mew : public Pokemon{
protected:
    string name;
    int type;
    vector<Attack*> attacks;

public:
    Mew(Type* type) : Pokemon("Mew", type){
        Pokemon::addAttack(new Attack("Psyko", type, 90));
        Pokemon::addAttack(new Attack("Choc Mental", type, 50));
    };
    ~Mew();

    string toString() const;

    string getName();
    Type getType();

    void setName(string newName);
    void setType(int newType);
};

#endif