#ifndef POKEMON_H
#define POKEMON_H

#include <iostream>
#include <string>
#include <vector>

#include "Type.h"
#include "Attack.h"

using namespace std;

class Pokemon
{
private:
    /* data */
protected:

    Type* type;
    string gender;
    string name;
    int16_t hp;
    int16_t maxHp;
    bool isKo = false;

public:
    std::vector<Attack*>array_attack;


    Pokemon()/* = delete*/;
    Pokemon(string name, Type* type);
    Pokemon(string name, Type* type, int maxHp);
    Pokemon(Pokemon const& pokemon);
    ~Pokemon();

    string toString() const;

    /* void takeDamage(int amount)
     * Permet de faire subire une certaine quantité de dégats au pokémon.
     */
    void takeDamage(int amount);

    /* string getName()
     * Permet d'obtenir le nom du pokémon.
     */
    string getName();

    /* string getGender()
     * Permet d'obtenir le genre du pokémon (male, femelle).
     */
    string getGender();

    /* string getType()
     * Permet d'obtenir le type du pokémon.
     */
    Type* getType();

    /* int getHp()
     * Permet d'obtenir les HP actuels pokémon.
     */
    int getHp();

    /* int getMaxHp()
     * Permet d'obtenir les HP max du pokémon.
     */
    int getMaxHp();

    bool getKo();

    /* void setName(string newName)
     * Permet de définir un nom pour un pokémon.
     */
    void setName(string newName);

    /* void setGender(string newGender)
     * Permet de définir un genre pour un pokémon.
     */
    void setGender(string newGender);

    /* void setType(string newType)
     * Permet de définir un type pour un pokémon.
     */
    void setType(Type* newtype);

    /* void addAttack(string attack)
     * Permet de d'ajouter une nouvelle attaque à un pokémon.
     */
    void addAttack(Attack* attack);

    /* void viewAttack()
     * Permet de voir les attaques d'un pokémon.
     */
    void setHp(int hp);

    /* void setHp(int hp)
     * Changes les HP actuels d'un pokémon.
     */
    void setMaxHp(int hp);

    /* void setMaxHp(int hp)
     * Changes les HP max d'un pokémon.
     */
    void viewAttack();

    void setKo(bool newKo);


    int16_t attack(Pokemon* pokemon, Attack* attaque);

    /* void heal(int amount)
     *  Permet de redonner 'amount' HP au pokemon (dans la limite de ces HP max)
     */
    void heal(int amount);
};


#endif