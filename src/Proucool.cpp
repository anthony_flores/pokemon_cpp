#include "PRoucool.h"
#include "Attack.h"

#include <iostream>
#include <string>
#include <Type.h>

using namespace std;

// Roucool::Roucool(){}

string Roucool::toString() const{
    return this->name + " de type Vol à " + to_string(this->attacks.size()) + " attaques";
}

Roucool::~Roucool(){}