#ifndef TYPE_EAU_INCLUDED_H
#define TYPE_EAU_INCLUDED_H

#include "Type.h"
#include "defs.h"

class TypeEau : public Type {    
public:
    TypeEau() : Type("Eau", EAU, PLANTE, FEU){};
    ~TypeEau();
};
#endif