#include "PCarapuce.h"
#include "Attack.h"

#include <iostream>
#include <string>
#include <Type.h>

using namespace std;

// Carapuce::Carapuce(){}

string Carapuce::toString() const{
    return this->name + " de type Eau à " + to_string(this->attacks.size()) + " attaques";
}

Carapuce::~Carapuce()
{}