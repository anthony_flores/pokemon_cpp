#include "PAspicot.h"
#include "Attack.h"

#include <iostream>
#include <string>
#include <Type.h>

using namespace std;

// Aspicot::Aspicot(){}

string Aspicot::toString() const{
    return this->name + " de type Insecte à " + to_string(this->attacks.size()) + " attaques";
}

Aspicot::~Aspicot(){}